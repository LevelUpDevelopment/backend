const passport = require('passport');

passport.use(new RegisterStrategy(
  function verify(username, password, done) {
    User.findOne({
      'username' : username
    }, function(err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(); // see section below
      }
      if (!user.verifyPassword(password)) {
        return done(null, false);
      }

      done(null, user);
    });
  }, function create(username, password, done) {
    User.create({
      'username' : username
    }, function(err, user) {
      if(err) {
        return done(err);
      }
      if(!user) {
        err = new Error("User creation failed.");
        return done(err);
      }

      done(null, user);
    });
  }
));
